# Unitrepo 1 | structure

Apache 2.0 (C) 2021 [librehof.com](http://librehof.com) [(top)](/README.md)


## Unit directories

(bold)=mandatory, (&#8594;)=separable  
handle config in separate repo, use some backup service for data/rawdata (and log)   

- **meta**/ (repo)
  - **label.var**
  - **version.var**
  - **date.var** # yyyymmddUTC  
  - **repoid.var** # random lowercase hex, md5-format
  - form.var # unit|platform|app|userapp
  - icon64.png
  - icon256.png
- default/ (repo)
  - \<default config\>
- doc/ (repo)
  - \<nodev documentation\>
- dev/ (repo)
  - \<dev scripts & resources, not available in release\>
- test/ (repo)
  - \<test scripts & resources\>
- \<other directory\>/ (repo/norepo)
- **local**/ (removed on teardown)
  - \<locally cached files\>
  - .update # if exist: marks request to update on next (re)start
  - .bound # if exist: yyyymmddUTChhmmss of last successful setup
- **config**/ &#8594;
  - **pointer.var** # if blank =\> filled with default branch
  - **origin.var** # blank =\> local (no git fetch, only checkout)
  - ordinal.var # none/blank =\> 0 (unique number 0..99 within host)  
  - import.tab # columns: name altname \[altorigin\]
  - artifact.tab # columns: name althash \[alturl\]
  - host.var # if union: only to be setup if matching name of host machine
  - update.var # if union: skip|offline
  - rev.var # optional "config schema" revision number 0..n
  - \[.\]service.systemd # systemd config (enabled if service.systemd exists and UNITUID=0)
  - \[.\]service.userd # systemd user config (enabled if service.userd exists and UNITUID is not 0)
  - \[.\]proxy.nginx # nginx config (enabled if proxy.nginx exists)
  - \[.\]proxy.apache # apache2 config (enabled if proxy.apache exists)
  - \[.\]syswall.apparmor # apparmor config (enabled if syswall.apparmor exists)
  - \[.\]deskicon.desktop # desktop icon
  - \[.\]menuicon.desktop # menu icon  
  - customsetup # custom setup sequence (inline [superscript](doc/superlib.md))
    - if unit supports this, unit's setup script will source this script (if it exists)
    - will be invoked before finalsetup
  - customteardown # custom teardown sequence (inline [superscript](doc/superlib.md))
    - if unit supports this, unit's teardown script will source this script (if it exists)
    - will be invoked after initialrelease
- **log**/ &#8594;
  - unit.log
  - app.log
  - \<log files\>
- data/ &#8594;
  - rev.var # optional "data schema" revision number 0..n
  - \<data files\>
- rawdata/ &#8594;
  - rev.var # optional "rawdata schema" revision number 0..n
  - \<raw data files\> # block files (random read/write access)


## Unit top files

- repo/:
  - **setup**
  - **teardown**
  - **unit**
  - **unitlib**
  - **superlib**
  - \<label\> # posix-launcher script (app/userapp)
  - \<label\>.bat # windows-launcher script (app/userapp)


## Unit import symlinking

- unit1/
- unit2/
- unit3/ - importing unit1 and unit2
  - config/
    - import.tab (optional import remapping)
  - somedir &#8594; ../unit1
  - anotherdir &#8594; ../unit2
  - setup - asking for unit1 and unit2 using importunit \<somedir\> \<unitname\> \<unitorigin\>


## Local artifact caching

- artifacts/ - shared directory with local artifacts (created on demand)
  - \<somename\>.\<lowercase-sha1\>
- unit1/
  - config/
    - artifact.tab (optional artifact remapping)
  - setup - asking for artifact file using getartifact \<somename\> \<sha1\> \<url\>


## Union with "data separation"

- artifacts/
  - \<somename\>.\<lowercase-sha1\>
- config/ \<== git repo defining union
  - \<units\>//
  - unionid.var # random lowercase hex, md5-format
- data/
  - \<units\>//
- rawdata/
  - \<units\>//
- log/
  - \<units\>//
- \<units\>//
- union # manage units
- superlib


## Unit symlinking when using "data separation"

- \<unit\>/
  - config &#8594; ../config/\<unit\>
  - data &#8594; ../data/\<unit\>
  - rawdata &#8594; ../rawdata/\<unit\>
  - log &#8594; ../log/\<unit\>
