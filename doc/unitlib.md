# Unitrepo 1 | unitlib

Apache 2.0 (C) 2021 [librehof.com](http://librehof.com) [(top)](/README.md)


## Standard config files

- service.systemd
- service.userd
- proxy.apache
- proxy.nginx
- syswall.apparmor


## Variables

- **available in script** and **can be used in standard config files**:
  - LABEL = meta/label.var
  - VERSION = meta/version.var
  - REPOID = meta/repoid.var
  - UNION = part of union \[true|false\]  
  - UNITPATH = absolute path to unit
  - UNIT = name of base directory
  - UNITOWNER = owner of unit's base directory (must match with user)
  - UNITUID = owner id
  - ORDINAL = use for separating apps on the same host \[0..99\]
  - NN = two-digit ordinal, use for local port assignment like 80${NN} 
  - HOSTNAME
  - RUNPATH = /run\[/user/*userid*\]
  - UNITPREFIX = <dotted unit path\>
    - unit's lock file = `RUNPATH/UNITPREFIX.lock`
    - launcher's pid/lock file = `RUNPATH/UNITPREFIX.LABEL.lock`
- **available after initialsetup** and **can be used in standard config files**:
  - SYSTEMSH = true path to original sh
  - CONFINEDSH = true path to unit's hard-linked sh used for apparmor confinement
  - DOTTEDSH = CONFINEDSH converted to dotted name (used in apparmor profile) 
  - UNITLAUNCH = launch string excluding arguments
    - unconfined: unitpath/label
    - confined: CONFINEDSH unitpath/label
- **available in script** but not in standard config files:
  - all [superlib](superlib.md) variables


## Import

```
#!/bin/sh
SCRIPT="$(basename "${0}")"
SCRIPTPATH="$(dirname "${0}")"
. "${SCRIPTPATH}/superlib"
cd "${SCRIPTPATH}"
. "./unitlib"
```


## Repo functions

```getvcs```
- outputs **git** if .git, **none** if .novcs
- error: message with return 2

```getorigin```
- ensures "config/origin.var"
- want = configured origin (want=have if blank)
- have = effective origin, blank if local
- error: message with return 2

```isintact```
- returns 0 if workspace is intact, 1 if modified
- error: message with return 2

```getlocalhash```
- last = hash at last update/sync = cached remote hash (blank if none)
- have = local hash (blank if repo has no commits yet)
- error: message with return 2

```getremotehash```
- outputs current remote hash given "pointer"
- blank if pointer does not exist (or pointer is an existing unlabeled hash)
- error: message with return 2

```getlocalpointer```
- ensures "config/pointer.var"
- want = configured pointer (want=have if blank)
- have = local pointer (or blank)
- error: message with return 2

```getsyncstate [offline]```
- sign =
  - `"?"` # origin mismatch
  - `"+"` # workspace changed
  - `"-"` # remote != local or pointer mismatch, needs **update**
  - `""` # remote = local
- localhash = local hash (not including workspace changes)
- lasthash = hash of last update/sync (hash of cached remote)
- remotehash = blank or verified remote hash
- error: message with return 2

```repoinfo```
- output repo multi-line info
- error: message with return 2


## Import functions

```getartifact <name> <sha1> <url>```
- cache/get artifact (file)
- outputs path to cached artifact file
- error: message with return 2

```importunit linkdir name <origin>```
- ensure local unit for final setup
- on final setup: linkdir/setup
- error: message with return 2


## State functions

```listimporters```
- abspath to all units having a link to this unit
- recursive, may contain duplicates
- error: message with return 2

```listbound```
- abspath to all units having a link to this and being bound/active
- recursive, no duplicates
- error: message with return 2

```listactive```
- abspath to all units having a link to this unit and being active
- recursive, no duplicates
- error: message with return 2

```listimports```
- abspath to linked imports
- error: message with return 2

```getconfinedsh```
- systemsh=\<true path to original sh\>
- confinedsh=\<true path where to put unit's confined sh\>
- dottedsh=\<path converted to apparmor profile name\>
- unit's confined sh (sh.\<unit\>) will be put alongside original sh
- error: message with return 2

```unitpids [skipself]```
- output pid list (group pids) of all active instances of this unit
- skipself: exclude ourself

```unitstate```
- active|bound|local|bare (see [flow](flow.md))
- error: message with return 2

```stopunit```
- go from active to bound
- error: message with return 2

```releaseunit [check]```
- go from active/bound to local
- check: will fail if any importer is bound/active (recursively)
- error: message with return 2

```updateunit [offline]```
- take unit's repo state to **configured** pointer (config/pointer.var)
- if state is bound or active, unit will be re-setup after update  
- see [update flow](flow.md#update-flow)


## Config and Data functions

```configdefault```

```resolveconfigfile```

```establishconfig```

```establishlog```

```ensurelocal```

```establishdata```

```establishrawdata```


## Setup and Teardown functions

```initialsetup <method1> <method2> <method3>```
- will exit with message on error

```finalsetup```
- will exit with message on error

```initialteardown [all]```
- will exit with message on error

```finalteardown```
- will exit with message on error
