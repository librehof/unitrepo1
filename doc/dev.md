# Unitrepo 1 | dev

Apache 2.0 (C) 2021 [librehof.com](http://librehof.com) [(top)](/README.md)


### Library dependencies

- When you are dealing with a **larger** library
  - Install systemwide using system's package manager
  - Or use the *platform unit* mechanism ([importunit](unitlib.md#import-functions))
  - Or use the *artifact* mechanism ([getartifact](unitlib.md#import-functions)):
    - unzip/copy inside unit directory when doing setup
    - make sure to add the resulting directory/files to the ignore-list
- When you are dealing with a **smaller** library (a few source files)
  - use an "inherit script" (dev/inherit) to routinely copy-in dependent files during development (embed)
  - the dependent files will be handled like any other source file, except they are changed via the "inherit script"


### Confinement

- uses apparmor (available for debian/ubuntu and others)
- sh.\<unit\> is created as a hard-link to sh (in the same directory as sh)
- unit must be launched using: `sh.<unit> <unitpath>/<launcher>`
- this will be setup automatically if you run as a service  
- default rules (apparmor profile):
  - general os rules for /etc,/bin,/var,/run,/proc,/dev,...
  - all imports are made available read-only TODO
  - config is writable except TODO
  - local is writable except TODO
  - data and rawdata is writable
  - RUNPATH/UNITPREFIX\* is writable (in-memory files)
  - \<custom rules\>
- what it protects against:
  - it may not protect the app itself, but rather other apps and the underlying system
  - this feature is mostly for protecting against missconfiguration or nasty bugs
  - it may also defend against non-sophisticated breakout attacks = you are a low value target    
- for full "containment" consider using lxc or docker and add firewall rules


### TODO

- system service:
  - service name: UNIT becomes a system/user global name, is that ok?
- apparmor:
  - improve apparmor profile
  - apparmor name: UNIT becomes a system global name, is that ok? ordinal?
- proxy service:
  - proxy config name: UNIT becomes a system global name, is that ok? ordinal?
- user setup:
  - userlocal service
    - what happens on ssh login?
    - what happens when user login twice? is it always the same session?
    - try to do a simple version on windows using its scheduler (launch on login)
- test:
  - update offline
  - change config/pointer.var and update
  - test what works in git-for-windows
- which environment variables should be exported/not exported (?)  
