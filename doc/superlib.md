## Superscript 1 | superlib

Apache 2.0 (C) 2021 [librehof.com](http://librehof.com) [(top)](/README.md)


### Categories

- <tt>I</tt>
  - [Time](#time)
  - [Key stroke](#key-stroke)
  - [String functions](#string-functions)
  - [Find in string](#find-in-string)
  - [Field functions](#field-functions)
  - [Replace functions](#replace-functions)
  - [Write line](#write-line)
  - [File size](#file-size)
  - [File-system functions](#file-system-functions)
  - [Lock](#lock)
  - [Hash functions](#hash-and-id-functions)
  - [Test functions](#test-functions)
  - [Check functions with error message](#check-functions-with-error-message)

- <tt>II</tt>
  - [Download and Installation](#download-and-installation)

- <tt>III</tt>
  - [General logging](#general-logging)
  - [General reporting](#general-reporting)
  - [Action reporting](#action-reporting)
  - [Large script reporting](#large-script-reporting)
  - [Exit with reporting](#exit-with-reporting)


### Import

- import from same directory (superlib copied to your script's location):
  ```
  #!/bin/sh
  SCRIPT="$(basename "${0}")"
  SCRIPTPATH="$(dirname "${0}")"
  . "${SCRIPTPATH}/superlib"
  # cd "${SCRIPTPATH}"
  ```

- import from another directory (relative to script):
  ```
  #!/bin/sh
  SCRIPT="$(basename "${0}")"
  SCRIPTPATH="$(dirname "${0}")"
  SUPERPATH="${SCRIPTPATH}/<relative path>"
  . "${SUPERPATH}/superlib"
  # cd "${SCRIPTPATH}"
  ```

- system wide import:
  ```
  #!/bin/sh
  SCRIPT="$(basename "${0}")"
  SCRIPTPATH="$(dirname "${0}")"
  . "/bin/superlib"
  # cd "${SCRIPTPATH}"
  ```


### English only

- Error messages in "easy" english
- Will set a neutral locale (en_US.UTF-8 or C.UTF-8)


### Elevation (access level)

- | Level         | Condition                                               |
  |:--------------|:--------------------------------------------------------|
  | **user**      | user access, USERID != 0                                |
  | **superuser** | system access, USERID = 0                               |


### Locality

- | Locality        | Condition                                             |
  |:----------------|:------------------------------------------------------|
  | **userlocal**   | started as ordinary **user**, GRANDUID != 0<br>sub-scripts may run as **superuser**, elevating with sudo -E |
  | **systemwide**  | started as **superuser**, GRANDUID = 0                |


### Configurable variables

- non-inherited:

  | variable    | value (default)                                            |
  |:------------|:-----------------------------------------------------------|
  | SUPERPATH   | *SCRIPTPATH*, /bin or *blank*                              |
  | SCRIPTUNIT  | defaults to script's leaf directory                        |
  | SCRIPTNAME  | name of script (without .sh)                               |

- inherited

  | variable    | value (default)                                            |
  |:------------|:-----------------------------------------------------------|
  | TMPPATH     | /tmp                                                       |
  | RUNPATH     | /run\[/user/*GRANDPID*\] (*TMPPATH* if no /run)            |
  | LOGFILE     | *blank* (see ensurelog)                                    |
  | LOGLIMIT    | 100000 (log rotation limit in bytes)                       |
  | TIMEOUT     | 5 (general time-out in seconds)                            |
  | SUPERPM     | *auto* (apt, yum, dnf, pacman, zypper, apk or blank)       |
  | SUPERCMD    | *blank* (SUPERCMD=true enables CMDLINE log)                |
  | SUPERDIV    | 1 ([div](#large-script-reporting) suppressed if GRANDDEPTH\>SUPERDIV) |


### Non-configurable variables

- established before import

  | variable    | value                                                      |
  |:------------|:-----------------------------------------------------------|
  | SCRIPTPATH  | script's directory path (made absolute by superlib)        |
  | SCRIPT      | filename of script                                         |

- non-inherited:

  | variable    | value                                                      |
  |:------------|:-----------------------------------------------------------|
  | RUNPREFIX   | dotted *SCRIPTPATH*.*SCRIPTNAME*                           |
  | USERID      | $(id -u)                                                   |
  | USER        | $(id -u -n)                                                |
  | DIVFILE     | RUNPATH/GRANDPREFIX.div.RUNEND                             |
  | STDFILE     | RUNPATH/RUNPREFIX.std.RUNEND                               |
  | CMDLINE     | "SCRIPT *arguments*"                                       |
  | MIB         | 1048576 (1024*1024)                                        |
  | IFS         | newline (can be saved/restored)                            |
  | TTYSTRING   | tty config to restore on exit/abort                        |
  | SUPERLIB    | *version*                                                  |

- inherited:
  
  | variable    | value                                                      |
  |:------------|:-----------------------------------------------------------|
  | GRANDDEPTH  | 1..20 (1=grand-script)                                     |
  | GRANDPID    | process id of grand-script = process group id (pgid)       |
  | GRANDPREFIX | grand-script's RUNPREFIX                                   |
  | GRANDPATH   | grand-script's SCRIPTPATH                                  |
  | GRANDSCRIPT | grand-script's filename                                    |
  | GRANDNAME   | grand-script's SCRIPTNAME                                  |
  | GRANDUID    | *user id* of user that launched grand-script               |
  | GRANDUSER   | **userlocal**: "*logged in user*" (when elevated too) <br>**systemwide**: "*superuser*" (usually root) |
  | HOSTNAME    | machine label                                              |
  | LOGDAY      | utcday                                                     |
  | NEXTFD      | 3..8 (next file descriptor, used by filelock)              |
  | RUNEND      | pid\<grandpid\>                                            | 


### Files

- | file                               | comment                                     |
  |:-----------------------------------|:--------------------------------------------|
  | /run/super.pid                     | system-exclusive lock (see superlock)       |
  | /run/super.last                    | utcday of last package-manager refresh      |
  | *RUNPATH*/super.cmd                | CMDLINE capture if enabled                  |
  | *RUNPATH*/*GRANDPREFIX*.*RUNEND*   | contains grand pid (gpid)                   |
  | *RUNPATH*/*GRANDPREFIX*.div.*RUNEND* | avoid double [div](#large-script-reporting) if present |
  | *RUNPATH*/*RUNPREFIX*.std.*RUNEND* | temporary stderr capture file               |
  | *RUNPATH*/*RUNPREFIX*.\<any\>.*RUNEND* | small temporary file (in-memory)        |
  | *TMPPATH*/*RUNPREFIX*.\<any\>.*RUNEND* | large temporary file (\>100KiB)         |


### List instances of a grand script

`ls -1 ${RUNPATH}/${GRANDPREFIX}.pid*`


### Temporary file removal

- Temporary files will be removed on exit/abort (via trap)
  - `${RUNPATH}/${RUNPREFIX}.*${RUNEND}`
  - `${TMPPATH}/${RUNPREFIX}.*${RUNEND}`

<br>
<hr>

### Time

```DAY=$(utcday)```
- "yyyymmddU"

```TIME="$(utctime ["<format>"])```
- "yyyymmddUhhmmss"

```DAY="$(hostday)```
- "yyyymmdd"

```TIME="$(hosttime ["<format>"])```
- "yyyymmddThhmmss"


### Key stroke

```KEY="$(keyinput [timeout])"```
- outputs key or blank
- optional timeout in seconds (max 25)

```keywait [timeout]```
- waits for key
- optional timeout in seconds (max 25)


### String functions

```LEN=$(stringlen "<string>")```

```START="$(startstring "<string>" 1..n)"```

```END="$(endstring "<string>" 1..n)"```

```PART="$(substring "<string>" 1..n 1..n)"```

```UPPER="$(upperstring "<string>")"```

```LOWER="$(lowerstring "<string>")"```


### Find in string

```findforward "<string>" "<searchsubstring>" [1..n]```
- start=1 or ${3}
- pos=\<1-based position before substring\>
- found="$(substring "\<string\>" \<start\> \<pos\>)"
- return: 0=found, 1=not found

```findbackward "<string>" "<searchseparator>" [1..n]```
- findbackward only works with single-char separator (!)
- pos=\<1-based position after separator\>
- end=stringlen or ${3}
- found="$(substring "\<string\>" \<pos\> \<end\>)"
- return: 0=found, 1=not found


### Field functions

```FIELD="$(spacefield "<string>" <1..n>)"```
- pick space-separated field in string

```FIELD="$(tabfield "<string>" <1..n>)"```
- pick tab-separated field in string


### Replace functions

```NEWSTR="$(replaceinstring "<from>" "<to>" "<string>")"```

```replaceinfile "<from>" "<to>" "<file>" [sudo]```
- error: message with return 2


### Write line

```LINE="$(loadline "<file>")"```
- read single-line file
- error: message with return 2

```saveline "<string>" "<file>" [sudo]```
- newline is added to string, then written to new file
- error: message with return 2

```appendline "<string>" "<file>" [sudo]```
- newline is added to string, then appended to file
- file will be created if missing  
- error: message with return 2


### File size

```INFO=$(sizeinfo <bytes>)```
- if n \< 1000000
  - "n bytes"
- elif n is a multiple of MIB:
  - "m MiB"
- else:
  - "m.n MiB" (cut to one decimal)

```BYTES=$(bytesize "<bytes>|<MiB>M" ["<varname>"])```
- output size in bytes
- error: message with return 2

```BYTES=$(filespace "<file>")```
- occupied file space on disk, in bytes
- could be zero, even for a large file (sparse file)

```BYTES=$(filesize "<file>")```
- logical file size, in bytes


### File-system functions

```PATH="$(abspath "<path>")"```
- absolute path
- parent must exist
- error: message with return 2

```PATH="$(truepath "<path>")"```
- resolve links and output absolute path
- error: message with return 2

```ensuredir "<dir>" [<mode>]```
- parent must exist
- error: message with return 2
- on creation and superuser: 
  - owner = owner of parent dir
    
```ensurefile "<file>" [sudo]```
- directory must exist
- error: message with return 2  
- on creation and superuser: 
  - owner = owner of parent dir

```clearfile "<file>" [<sparsesize>[M]] [sudo]```
- directory must exist
- error: message with return 2  
- if superuser:
  - owner = owner of parent dir

```ensurenofile "<file>" [sudo]```
- error: message with return 2

```ensurecopy "<source>" "<target>" [sudo]```
- ensure file is same as source based on size and modification time
- error: message with return 2

```inheritowner "<path>" [sudo]```
- if superuser or sudo:
  - user = owner of parent dir
- error: message with return 2

```ensurelog "<logfile>"```
- LOGFILE=\<logfile\>
- log to syslog if LOGFILE="#\<systag\>"  
- error: LOGFILE="", message with return 2

```clearlog "<logfile>"```
- ensurelog \<logfile\>
- clearfile \<logfile\>
- error: LOGFILE="", message with return 2


### Lock

```filelock "<pidfile>" [timeout]```
- auto-unlocked on exit
- error: message with return 2

```superlock```
- check superuser and
- acquire systemwide lock: /run/super.pid (/tmp/super.pid)
- false/error: message with return 2


### Hash and ID functions

```HASH="$(md5hash "<file>")"```
- outputs 128-bit lowercase MD5 hex (32 characters)
- error: message with return 2

```HASH="$(sha1hash "<file>")"```
- outputs 160-bit lowercase SHA-1 hex (40 characters)
- error: message with return 2

```ID="$(genhexid)"```
- outputs 128-bit random lowercase hex (32 characters)


### Test functions

```isint "<possible integer>"```
- return: 0=true, 1=false

```iscmd "<system command>"```
- return: 0=true, 1=false


### Check functions with error message

```checkint "<possible integer>" ["<varname>"]```
- check if integer
- false/error: message with return 2

```checkfile "<path>"```
- check if file exists
- false/error: message with return 2

```checkdir "<path>"```
- check if directory exists
- false/error: message with return 2

```checkdevice "<block device>"```
- check if block device is present
- will give device 1 second to appear
- false/error: message with return 2

```checkcmd "<command>"```
- check command is available
- false/error: message with return 2

```checksuperuser```
- check if superuser
- false/error: message with return 2

<br>
<hr>

### Download and Installation

```httpdownload "<url>" "<path>" ["<md5/sha1>"]```
- curl is used for download (will ensure it is installed)
- error: message with return 2

```refreshpm```
- refresh package manager's index
- failure: warning with return 1
- error: message with return 2

```ensurepkg [<pm>:]<package>[+] ...```
- takes a list of package names (to cover multiple package managers)
- see [command-not-found.com](https://command-not-found.com) for package names
- success: return 0, package="\<package\>" if installed
- error: message with return 2

```ensurecmd "<command>" @ [<pm>:]<package>[+] ...```
- takes a command + list of package names (to cover multiple package managers)
- see [command-not-found.com](https://command-not-found.com) for package names
- success: return 0, package="\<package\>" if installed
- error: message with return 2

<br>
<hr>

### General logging

```filelog "<line>" "<logfile>" [<bytelimit>]```
- directory must exist, silent failure otherwise
- will copy "\<logfile\>" to "\<logfile\>.old" on rotation

```printlog "<line(s)>"```
- print to stderr (fd2) and log to LOGFILE (if set)
- log to syslog if LOGFILE="#\<systag\>"
- only print if LOGFILE=""


### General reporting

```info ["<message>"]```
- send info/progress message to stderr (fd2)
- not logged, to be viewed in a shell "when it happens"
 
```note "<message>"```
- printlog "\<message\>"

```notetail "<file>" [maxlines]```
- default maxlines=5
- if file exists:
  - printlog last line(s)

```warning "<message>"```
- printlog "WARNING: \<message\>"

```error "<message>"```
- printlog "ERROR: \<message\>"


### Large script reporting

```title ["<title>"]```
- note "[\<title\>]"
- surrounded by divs
- default title = \<SCRIPTNAME\>
- if GRANDDEPTH > SUPERDIV:
  - note "\<title\>"
  
```subtitle ["<title>"]```
- note "# \<title\>"
- surrounded by divs
- default title = "separator"
- if GRANDDEPTH > SUPERDIV:
  - note "\<title\>"

```separator ["<title>"]```
- 44-char tilde separator (with optional embedded title)
- surrounded by divs
- if GRANDDEPTH > SUPERDIV:
  - converted to subtitle

```div```
- blank info line (not logged)
- avoids double div:
  - if DIVFILE exists, div is skipped
- if GRANDDEPTH > SUPERDIV:
  - div is suppressed

```divnext```
- force next div by removing DIVFILE


### Action reporting

```action "Doing something"```
- printlog "\<message\>"

```success ["Done something"]```
- calls sync if grand script (writes buffered data to file)
- printlog "ok" or "\<message\>"


### Exit with reporting

```exit 0```
- success exit (default exit)

```exit 1```
- none/false/warning exit

```summaryexit ["<impact>"]```
- before calling: define summary() function with info lines
- impact = "" | "read" | "bind" | "write" | "read/\<other\>"
1. info "\<SCRIPT\> \[(\<impact\>)\]"
2. calls summary()
3. exit 99

```inputexit ["<hint>"]```
- printlog "hint" 
- or 
  - info \<CMDLINE\>
  - printlog "Illegal input (try --help)"
- exit 3

```errorexit ["<message>"]```
- \[printlog error and\] exit 2

```exitonerror <status> ["<message>"]```
- if status \>= 1:
  - \[printlog error and\] exit 2

<br>
<hr>
