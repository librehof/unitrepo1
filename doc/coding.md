## Superscript 1 | coding

Apache 2.0 (C) 2021 [librehof.com](http://librehof.com) [(top)](/README.md)


### Code patterns

POSIX shell scripts are very portable (may work on the most minimalistic systems)<br>
They can however be a bit tricky, output and error handling for example<br>
Below are some "code patterns" to get a good start...<br>

- upper-case for ordinary **script** variables

- lower-case for **internal** variables and **function output** variables

- blank/true/false/none:
  ```
  VAR=""|true|false|none
  
  if [ "${VAR}" = "" ]; then
    <do something on blank>
  fi

  if [ "${VAR}" = true ]; then
    <do something on true>
  fi
  
  if [ "${VAR}" != true ]; then
    <do something on not-true>
  fi
  
  if [ "${VAR}" = false ]; then
    <do something on false>
  fi
  
  if [ "${VAR}" = none ]; then
    <do something on none>
  fi
  ```

- number compare flags:
  ```
  -ge  # greater or equal (>=)
  -gt  # greater then (>)
  -le  # less or equal (=<)
  -lt  # less then (<)
  ```

- number:
  ```
  VAR=<number> # no quoting  
  
  # always quote variables inside condition
  if [ "${VAR}" = somenumber ]; then
    dosomething
  fi
  
  VAR=$((VAR1+VAR2+1)) # math expression (integers)
  
  if [ "${VAR}" -<flag> somenumber ]; then
    dosomething
  fi  
  ```

- string:
  ```
  VAR="somestring"
  if [ "${VAR}" = "somestring" ]; then
    dosomething
  fi
  ```

- variable expansion inside "string":
  ```
  NEWVAR="something ${VAR1} something ${VAR2}"
  ```

- 4 characters that needs escaping in "string":
  ```
  $ ==> \$ 
  ` ==> \` 
  \ ==> \\ 
  " ==> \"
  ```

- and/or:
  ```
  if [ "${VAR1}" = "some" ] && [ "${VAR2}" = "some" ]; then
    dosomething
  fi
  
  if [ "${VAR1}" = "some" ] || [ "${VAR2}" = "some" ]; then
    dosomething
  fi  
  ```

- if/else:
  ```
  if [ <condition> ]; then
    dosomething
  elif [ <condition> ]; then
    dosomething
  else
    dosomething
  fi
  ```

- while:
  ```
  while :
  do
    if [ <break condition> ]; then
      break
    fi
    dosomething
  done
  ```
  
- iterate:
  ```
  # example with newline separtor (default)
  FILES="$(ls -1 "./" 2> /dev/null)"
  exitonerror ${?} "Could not list files in current directory"
  for FILE in ${FILES}
  do
    <do something with ${FILE}>
  done
  
  # with separator other then newline
  OLDIFS="${IFS}"
  IFS="<separtor>"
  for var in ${something}
  do
    <do something with ${var}>
  done
  IFS="${OLDIFS}"
  ```

- script's input arguments:
  ```
  ARG1="${1}"
  ARG2="${2}"
  ...
  ```

- output of result (fd=1):
  ```
  # script/function direct output (fd1)
  echo "someconstant" # with newline (only for constants)
  printf '%s' "${VAR}" # without newline
  printf '%s\n' "${VAR}" # with newline
  
  # function multi-variable output 
  # do NOT combine with direct output!
  somefunction()
  {
    <do something>
    someoutputvar1="somestring"    
    someoutputvar2=somenumber
    return 0    
  }
  ```

- function:
  ```
  # always return with explicit: return <code> 
  # status codes: 0=success/true, 1=false/none/warning, 2=error
  # do NOT use exit inside a function! always use return
  
  somefunction()
  {
    arg1="${1}"
    arg2="${2}"
    someinternalvar="something"
    if [ <exception condition> ]; then
      return 1 # none/false/warning (status 1)
    fi
    if [ <error condition> ]; then
      error "Some error"
      return 2
    fi
    printf '%s' "<some direct output>"
    return 0 # explicit return even on success (status 0)
  }
  ```

- boolean function:
  ```  
  issomefunction()
  {
    arg1="${1}"
    arg2="${2}"
    if [ <condition> ]; then
      return 0 # true
    else
      return 1 # false
    fi
  }
  ```

- filesystem test flags:
  ```
  -e exists
  -x excutable file / accessible directory
  -d directory
  -f datafile
  -b blockdevice
  -c chardevice
  -L symlink
  ```

- filesystem test:
  ```
  if [ -<flag> "<path>" ]; then
    dosomething
  fi
  
  if [ ! -<flag> "<path>" ]; then
    dosomething
  fi
  ```


### Calling (output and error handling)

- Standard streams:
  ```
  stdout (fd1): direct output, suppress with 1> /dev/null
  stderr (fd2): action reporting, ends with error message on failure
  ```
  
- General exit codes for **most** commands:
  ```
  0 = success
  1-255 = error
  ```

- General exit codes for **some** commands:
  ```
  0 = success/true
  1 = none/false/warning
  2-255 = error
  ```

- Error codes for superscripts:
  ```
  2 = general error
  3 = input error (with hint, or how to print help)
  99 = output contains help text (-h or --help)
  126 = execution denied
  127 = command not found
  128+signal = terminated by signal
  ```

- Error functions:
  ```
  error "<message>"  # report error
    
  errorexit ["<message>"]  # [report error and] exit 2
  
  exitonerror <status> ["<message>"]  # if >= 1 errorexit
  ```

- Call a **superscript** (sub-script):
  ```
  # print output (pass through), display progress
  somescript
  exitonerror ${?}

  # suppress output, display progress
  somescript 1> /dev/null
  exitonerror ${?}
  
  # capture output, display progress
  OUTPUT="$(<script>)"
  exitonerror ${?} 
  ```

- Call an ordinary **command**:
  ```
  # suppress output and progress   
  somecommand 1> /dev/null 2> /dev/null
  exitonerror ${?} "Failed to ..."
  
  # capture output, suppress progress
  OUTPUT="$(somecommand 2> /dev/null)
  exitonerror ${?} "Failed to ..."

  # use superlib's STDFILE to log "reason" for error
  clearfile "${STDFILE}"
  <command> 1> /dev/null 2>> "${STDFILE}"
  if [ "${?}" != 0 ]; then
    notetail "${STDFILE}"
    errorexit "Failed to ..."
  fi
  ```

- Call a script **function**:
  ```
  # without output
  somefunction "somestring" somenumber
  
  # with direct output
  OUTPUT="$(somefunction "somestring" somenumber)"
  
  # with boolean return code 
  issomefunction "somestring" somenumber
  if [ "${?}" = 1 ]; then
    <do something on false>
  fi
  
  # with boolean return code, function may produce errors
  issomefunction "somestring" somenumber
  status=${?}
  if [ "${status}" -ge 2 ]; then
    errorexit
  fi
  if [ "${status}" = 1 ]; then
    <do something on false>
  fi
  ```

- Ensure something exists (idempotence):
  ```
  # in this example status 1 = not found
  FOUND="$(somecommand "<searchforsomething>")"
  if [ "${?}" -ge 2 ]; then
    errorexit
  fi
  if [ "${FOUND}" = "" ]; then
    createwhatwasnotfound
  fi
  ```

- Call with status code checking:
  ```
  runsomecommand
  status=${?} # status code 0-255
  if [ "${status}" = 1 ]; then
    somespecialcase
  elif [ "${status}" = 2 ]; then
    somespecialcase
  elif [ "${status}" -ge 3 ]; then
    someerror
  fi
  ```


### Action reporting

- ```
  action "Doing ..."  

  dosomething
  
  action "Doing ..."

  dosomething
  
  success "Done ..." # disk sync
  ```


### Large script reporting

- ```
  title
  
  subtitle "first"
  
  <actions>
  
  subtitle "second"
  
  <actions>
  
  subtitle "end"
  ```


### Execution reporting

- Report progress (not logged)
  ```
  action "Doing ..."
  while :
  do
    if [ <break condition> ]; then
      break
    fi
    dosomething
    info "some progress"
  done
  ```

- Log execution information
  ```
  if [ <condidion> ]; then
    note "Something to be noticed"
    <do something special>
  fi

- Log problem and continue
  ```
  action "Doing ..."
  if [ <condidion> ]; then
    warning "Some potential problem"
  fi
  dosomething
  ```
