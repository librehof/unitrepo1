# Unitrepo 1 | flow

Apache 2.0 (C) 2021 [librehof.com](http://librehof.com) [(top)](/README.md)

<br>

## Unit states
<br>

```mermaid
graph LR
  subgraph unready
    bare-->local
  end  
  subgraph ready
    local-->bound
    bound-->active
  end
  style bare fill:#ffffff,stroke:#404040,stroke-width:2px
  style local fill:#80ffff,stroke:#404040,stroke-width:2px,stroke-dasharray:4
  style bound fill:#80ff00,stroke:#404040,stroke-width:2px
  style active fill:#ffff00,stroke:#404040,stroke-width:2px,stroke-dasharray:4
```
<br>

conditions are added (from left to the right)  

- **bare** - updatable
  - checked out
  - possible log
  - untouched config and data
  - no local directory (removed)
  - no imports (unlinked)
  - after teardown:
      - possibly left over build material (if time-consuming to build)
  - after teardown all:
    - no left over build material
- **local** - locally prepared for final setup, config is ok
  - **local** directory
  - log directory
  - verified/migrated config directory
  - ensured imports & artifacts
  - locally built/optimized (when applicable)  
  - untouched data
- **bound** - ready for use, bound to data (if any)
  - **local/.bound** = yyyymmddUTChhmmss
  - verified/migrated data
  - possibly in-use by system/importers
  - not active by itself (importers might be active)
- **active**
  - launched, having running process(es)

<br>

## Noapp flow
<br>

```mermaid
sequenceDiagram
  participant bare
  participant local
  participant bound
  participant active
  bare->>local: setup deps
  bare->>bound: setup
  bound->>local: release (recursive)
  bound->>bare: teardown / terdown all
```

<br>

Note: teardown requires all importers (if exists) to be released first

<br>

## App flow
<br>

```mermaid
sequenceDiagram
  participant bare
  participant local
  participant bound
  participant active
  bare->>local: setup deps
  bare->>bound: setup (manual)
  active->>bound: setup (manual)
  bare->>active: setup (service)
  bound-->>active: launch
  active-->>bound: stop
  active->>local: release (recursive)
  active->>bare: teardown / teardown all
```

<br>

Note 1: teardown requires all importers (if exists) to be released first  
Note 2: explicit `setup manual` and `setup service` changes config (affects next setup)

<br>

## Update flow
<br>

```mermaid
sequenceDiagram
  participant bare
  participant local
  participant bound
  participant active
  active->>bare: 1. teardown
  bare->>bare: 2. fetch/checkout
  bare->>local: 3. setup deps
  local->>bound: 4. setup (noapp)
  local->>bound: 4. setup (manual)
  local->>active: 4. setup (service)
```

<br>

Note 1: if unit's state is not bound/active before update, step 3 and 4 will be skipped (!)  
Note 2: when updating a single unit, all importers must be released first, then setup again  
Note 3: workspace must be unmodified, or update will fail

<br>
<br>
<br>
