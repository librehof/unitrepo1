## Unitrepo 1

Apache 2.0 (C) 2021 [librehof.com](http://librehof.com)


### Summary

- Use your **Version Control System** (git) for **direct deploy**
- Update works on linux (with git installed) and on windows (with [git for windows](https://gitforwindows.org))
- Optionally integrates with systemd, apparmor, nginx and apache2 (linux only)
- Built on top of [superscript](http://librehof.com)
- Can be used together with [unionrepo](http://librehof.com)
- [Legal notice](NOTICE)


### Details

- **[structure](doc/structure.md)**
- **[flow](doc/flow.md)**
- [dev](doc/dev.md)
- [unitlib](doc/unitlib.md)
- [superlib](doc/superlib.md) ([coding](doc/coding.md))
- [changelog](doc/changelog.md)


### Get

```
# download
git clone --depth 10 https://gitlab.com/librehof/unitrepo1.git
cd unitrepo1

# update
cd unitrepo1
git pull --depth 10

# remove
rm -rf unitrepo1
```


### Adopt

To make your repo a **unit** repo:
```
NEWLABEL="<short lowercase name of your app>"
NEWREPO="<name of your repo>"

# create new git repo (if needed)
mkdir -p "${NEWREPO}"
cd "${NEWREPO}"
git init
<create remote repo>
git remote add origin <url of your new repo>
git push -u origin <name of your default branch>
cd ..

# clone this repo (if needed)
git clone --depth 10 https://gitlab.com/librehof/unitrepo1.git

# modify your repo...

cp "unionrepo1/setup" "${NEWREPO}/"
cp "unionrepo1/teardown" "${NEWREPO}/"
cp "unionrepo1/unit" "${NEWREPO}/"
cp "unionrepo1/unitlib" "${NEWREPO}/"
cp "unionrepo1/superlib" "${NEWREPO}/"
cp "unionrepo1/.gitignore" "${NEWREPO}/.gitignore"

mkdir -p "${NEWREPO}/meta"
echo "${NEWLABEL}" > "${NEWREPO}/meta/label.var"

# optionally create default config directory
mkdir -p "${NEWREPO}/default"
cp "unionrepo1/default/*" "${NEWREPO}/default/"

# optionally provide launcher script (edit afterwords)
cp "unionrepo1/unitrepo" "${NEWREPO}/${NEWLABEL}"

cd "${NEWREPO}"
./unit state
# now review the meta directory
git add .
git commit -m "unitrepo"
git push
./unit info
```


### Unit forms

- unit
  - generic unit
  - no launcher script
- app    
  - has launcher script (name = content of meta/label.var)  
  - can be setup as a systemwide service or userlocal service 
- userapp
  - usually a desktop-like application
  - has launcher script (name = content of meta/label.var)
  - launched manually by user (one or possibly several times)
  - can not be setup as a service (no auto start)
  - always userlocal (systemwide not supported)
- platform
  - large and shared library
  - shall not have any "instance" data of its own
  - see [library dependencies](doc/dev.md#library-dependencies)


### Commands

- `./setup`
  - idempotent
  - to setup anew: teardown first
- `./setup deps`
  - ensure dependencies, migrate config, compile/optimize
  - do not touch data/rawdata
  - do not setup service
- `./setup service|manual`
  - systemwide: enable+start (and auto start on boot) or stop+disable 
  - userlocal: enable+start (and auto start on first login) or stop+disable
- `./setup service|manual syswall|nosyswall`
  - app: enable/disable apparmor profile (userlocal requires sudo access)
- `./setup proxy|noproxy`
  - app: enable/disable nginx or apache2 proxy (userlocal requires sudo access)
- `./setup menuicon|nomenuicon`
  - app: enable/disable icon in menu (userlocal only)
- `./setup deskicon|nodeskicon`
  - app: enable/disable icon on desktop (userlocal only)
- `./teardown [all]`
  - requires `./unit release` first if bound importers exists   
  - removes local directory
  - *all*: removes all intermediate material
- `./unit <command>`
  - run `./unit -h` for help


### Update

- run `./unit update`
- or inside app (dev) 
  - create `local/.update` file and do exit 1
  - exit 1 requests launcher to restart
  - may not work when using apparmor
- see [update flow](doc/flow.md#update-flow)


### Setups

- **System-boot service** (linux only)
  - Default location: /srv/\<unit\>
  - Running as superuser
  - Optionally confined using apparmor
- **User-login service**
  - Default location: \<userdir\>/app/\<unit\>
  - Launched on first login
    - TODO: Support windows (by setting up a scheduler login task) 
  - Optionally confined using apparmor (requires sudo access when running setup)

